# Usine marémotrice

Projet de modélisation de l'usine marémotrice de la Rance avec Modelica

par Emna Hnichi, Antoine Fraignaud et Philippine Rous

Notre objectif était de modéliser le fonctionnement de l'usine marémotrice de la Rance en représentant au cours du temps les hauteurs du bassin et de la mer ainsi que la puissance générée par l'usine.

# Sructure du code

Dans notre package complet "Tidal_Project", on retrouve 2 sous-package, l'environnement, le modèle de l'usine complète et la simulation: 
- Composants : contient les composants principaux de l'usine nécessaire à sa modélisation, c'est à dire les turbines, les vannes et le bloc de commande
- Interfaces : contient 2 types de port nécessaire à notre modélisation, ici "FluidPort" permet de recueillir la hauteur d'eau et le flux à l'interface et "SignalPort" permet de recueillir un signal de commande.
- Bassin, Mer : ce sont l'environnement de notre usine
- Usine : c'est le modèle complet de notre usine qui regroupe les 3 composants et l'environnement de l'usine, reliés grace aux 2 types de ports créés
- Simulation

# Faire la simulation

- Dans le fichier "Tidal_Project", ouvrir "package.mo" pour ouvrir le code complet
- Dans OMEdit, ouvrir "Simulation"
- Lancer la simulation
- Afficher bassin/S_bassin/s et mer/S_mer/s pour voir le niveau d'eau du bassin et de la mer
- Afficher turbine/P pour observer la puissance produite
- Observer command/S_Turbines/s et command/S_Vannes/s pour observer les changements d'état des composants
