within Tidal_Project;

model Bassin "Le réservoir de l'usine marémotrice de la Rance"
  parameter Real A = 1500 "Surface du réservoir";
  Modelica.Units.SI.VolumeFlowRate Q;
  Interfaces.SignalPort S_Bassin annotation(
    Placement(visible = true, transformation(origin = {0, -100}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {0, -100}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Interfaces.FluidPort turbines annotation(
    Placement(visible = true, transformation(origin = {-100, 40}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-100, 40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal_Project.Interfaces.FluidPort vannes annotation(
    Placement(visible = true, transformation(origin = {-100, -40}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-100, -38}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  Q = turbines.Qv + vannes.Qv;
  A * der(S_Bassin.s) = Q;
  turbines.h = S_Bassin.s;
  vannes.h = S_Bassin.s;
  annotation(
    Icon(graphics = {Rectangle(origin = {0, -17}, lineColor = {0, 170, 255}, fillColor = {0, 255, 255}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-100, -83}, {100, 83}}, radius = 25), Rectangle(origin = {-87, -15}, lineColor = {85, 0, 0}, fillColor = {85, 0, 0}, fillPattern = FillPattern.Solid, extent = {{-13, 85}, {13, -85}}), Rectangle(origin = {87, -15}, lineColor = {85, 0, 0}, fillColor = {85, 0, 0}, fillPattern = FillPattern.Solid, extent = {{-13, 85}, {13, -85}}), Rectangle(origin = {0, -85}, lineColor = {85, 0, 0}, fillColor = {85, 0, 0}, fillPattern = FillPattern.Solid, extent = {{-100, 15}, {100, -15}})}));
end Bassin;
