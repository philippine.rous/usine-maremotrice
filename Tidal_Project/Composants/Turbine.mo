within Tidal_Project.Composants;

model Turbine

  Modelica.Units.SI.Length H "chute d'eau";
  Modelica.Units.SI.VolumeFlowRate Q "débit";
  Tidal_Project.Interfaces.FluidPort mer annotation(
    Placement(visible = true, transformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));


  parameter Real alpha_d=1820 "sens direct (bassin vers mer)";
  parameter Real alpha_i=-1420 "sens indirect (mer vers bassin)";
  parameter Modelica.Units.SI.Density rho=998.2 "densité de l'eau salée";
  parameter Modelica.Units.SI.Acceleration g=9.81 "constante d'accéleration";
  parameter Real Qconstcouplage=3000;
  parameter Real Qconstpompage=-3000;
  Modelica.Units.SI.Power P "puissance";
  Tidal_Project.Interfaces.SignalPort S_Turbines annotation(
    Placement(visible = true, transformation(origin = {0, -100}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-2, -100}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal_Project.Interfaces.FluidPort bassin annotation(
    Placement(visible = true, transformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  H = bassin.h - mer.h;
//calcul de la hauteur de chute
  if integer(S_Turbines.s) == 0 then
//turbines fermées
    Q = 0;
    P = 0;
  elseif integer(S_Turbines.s) == 1 then
//turbines à vide
    if H > 0 then
      Q = 24*alpha_d * sqrt(abs(H));
    else
      Q = 24*alpha_i * sqrt(abs(H));
    end if;
    P = 0;
  elseif integer(S_Turbines.s) == 2 then
//turbines couplées
    if H > 0 then
      Q = Qconstcouplage;
    else
      Q = -Qconstcouplage;
    end if;
    P = rho * g * H * abs(Q);
  else
//pompage
    Q = Qconstpompage;
    P = rho * g * H * Q;
  end if;
  Q = bassin.Qv;
  bassin.Qv = mer.Qv;
//égalité des flux
  annotation(
    Icon(graphics = {Ellipse(extent = {{-25, 25}, {25, -25}}), Polygon(origin = {3, -42}, rotation = 140, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Polygon(origin = {-11, -40}, rotation = 120, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Polygon(origin = {-25, -34}, rotation = 100, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Polygon(origin = {-35, -22}, rotation = 80, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Polygon(origin = {-41, -10}, rotation = 60, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Polygon(origin = {-43, 6}, rotation = 40, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Polygon(origin = {-37, 20}, rotation = 20, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Polygon(origin = {-29, 30}, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Polygon(origin = {-15, 36}, rotation = 340, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Polygon(origin = {-1, 42}, rotation = 320, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Polygon(origin = {15, 40}, rotation = 300, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Polygon(origin = {27, 32}, rotation = 280, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Polygon(origin = {39, 22}, rotation = 260, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Polygon(origin = {45, 8}, rotation = 240, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Polygon(origin = {43, -6}, rotation = 220, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Polygon(origin = {37, -20}, rotation = 200, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Polygon(origin = {29, -34}, rotation = 180, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Polygon(origin = {17, -44}, rotation = 160, fillColor = {122, 122, 122}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, points = {{29, -32}, {29, -32}, {-51, 36}, {-23, 38}, {3, 22}, {17, 6}, {23, -6}, {27, -20}, {29, -32}}, smooth = Smooth.Bezier), Ellipse(fillColor = {112, 112, 112}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, extent = {{-35, 35}, {35, -35}})}));
end Turbine;
