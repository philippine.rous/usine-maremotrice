within Tidal_Project.Interfaces;

connector FluidPort
  Modelica.Units.SI.Height h "Hauteur d'eau";
  flow Modelica.Units.SI.VolumeFlowRate Qv(displayUnit = "m3/h") "Debit Volumique";
annotation(
    Icon(graphics = {Rectangle(fillColor = {144, 0, 0}, fillPattern = FillPattern.Solid, extent = {{-100, -100}, {100, 100}})}),
    Diagram(graphics = {Rectangle(fillColor = {144, 0, 0}, fillPattern = FillPattern.Solid, extent = {{-40, -40}, {40, 40}}), Text(origin = {0, 80}, extent = {{-140, -20}, {140, 20}}, textString = "%name")}));
end FluidPort;
