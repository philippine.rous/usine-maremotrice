within Tidal_Project.Interfaces;

connector SignalPort
  Real s "Signal de commande";
  annotation(
    Diagram(graphics = {Rectangle(fillColor = {0, 170, 0}, fillPattern = FillPattern.Solid, extent = {{-40, -40}, {40, 40}}), Text(origin = {0, 80}, extent = {{-140, -20}, {140, 20}}, textString = "%name")}),
    Icon(graphics = {Rectangle(fillColor = {0, 255, 127}, fillPattern = FillPattern.Solid, extent = {{-98, 98}, {98, -98}})}));
end SignalPort;
