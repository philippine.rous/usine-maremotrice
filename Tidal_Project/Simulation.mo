within Tidal_Project;

model Simulation

  Tidal_Project.Mer mer annotation(
    Placement(visible = true, transformation(origin = {-74, 2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal_Project.Bassin bassin annotation(
    Placement(visible = true, transformation(origin = {68, 2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal_Project.Composants.Command command annotation(
    Placement(visible = true, transformation(origin = {-2, -52}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal_Project.Composants.Turbine turbine annotation(
    Placement(visible = true, transformation(origin = {-16, 44}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Tidal_Project.Composants.Vannes vannes annotation(
    Placement(visible = true, transformation(origin = {2, -2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));

equation
  connect(mer.S_Mer, command.S_Mer) annotation(
    Line(points = {{-74, -8}, {-74, -52}, {-12, -52}}));
  connect(command.S_Turbines, turbine.S_Turbines) annotation(
    Line(points = {{-6, -42}, {-16, -42}, {-16, 34}}));
  connect(command.S_Vannes, vannes.S_Vannes) annotation(
    Line(points = {{2, -42}, {2, -12}}));
  connect(command.S_Bassin, bassin.S_Bassin) annotation(
    Line(points = {{8, -52}, {68, -52}, {68, -8}}));
  connect(bassin.vannes, vannes.bassin) annotation(
    Line(points = {{58, -2}, {12, -2}}));
  connect(vannes.mer, mer.vannes) annotation(
    Line(points = {{-8, -2}, {-64, -2}}));
  connect(mer.turbines, turbine.mer) annotation(
    Line(points = {{-64, 6}, {-26, 6}, {-26, 44}}));
  connect(turbine.bassin, bassin.turbines) annotation(
    Line(points = {{-6, 44}, {58, 44}, {58, 6}}));
  annotation(
    experiment(StartTime = 0, StopTime = 30, Interval = 0.02),
    Icon(graphics = {Line(points = {{-40, -69.282}, {-30, -51.962}}), Ellipse(extent = {{-80, -80}, {80, 80}}), Line(points = {{80, 0}, {60, 0}}), Line(points = {{-69.282, 40}, {-51.962, 30}}), Line(points = {{69.282, 40}, {51.962, 30}}), Line(points = {{0, -80}, {0, -60}}), Line(points = {{-69.282, -40}, {-51.962, -30}}), Line(origin = {0, -0.447761}, points = {{0, 0}, {-50, 50}}), Rectangle(lineColor = {200, 200, 200}, fillColor = {248, 248, 248}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-100, -100}, {100, 100}}, radius = 25), Line(points = {{69.282, -40}, {51.962, -30}}), Line(points = {{-40, 69.282}, {-30, 51.962}}), Rectangle(lineColor = {128, 128, 128}, extent = {{-100, -100}, {100, 100}}, radius = 25), Line(points = {{40, -69.282}, {30, -51.962}}), Line(points = {{0, 0}, {40, 0}}), Line(points = {{-80, 0}, {-60, 0}}), Line(points = {{40, 69.282}, {30, 51.962}}), Line(origin = {-0.268652, 0.149254}, points = {{0, 0}, {-50, 50}}), Line(origin = {0.149254, -0.298507}, points = {{80, 0}, {60, 0}}), Ellipse(extent = {{-80, -80}, {80, 80}}), Line(origin = {0.597015, 0.149254}, points = {{69.282, 40}, {51.962, 30}}), Line(origin = {0.149254, 0.597018}, points = {{0, 80}, {0, 60}}), Line(origin = {0.149254, 0.149254}, points = {{-69.282, 40}, {-51.962, 30}}), Line(origin = {-0.298507, -0.298507}, points = {{-69.282, -40}, {-51.962, -30}}), Line(origin = {0.149254, -0.298507}, points = {{-40, -69.282}, {-30, -51.962}}), Line(origin = {0.149254, 0.149254}, points = {{0, -80}, {0, -60}})}),
  Diagram(graphics = {Text(origin = {-1, 82}, fillColor = {0, 85, 255}, fillPattern = FillPattern.Solid, extent = {{-63, 12}, {63, -12}}, textString = "SIMULATION", fontName = "Arial", textStyle = {TextStyle.Bold}), Text(origin = {-1, -78}, extent = {{-95, 14}, {95, -14}}, textString = "observez S_Mer.s, S_Bassin.s et P", fontName = "Arial")}));

end Simulation;
