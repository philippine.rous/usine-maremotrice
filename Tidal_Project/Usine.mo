within Tidal_Project;

model Usine
  Mer mer annotation(
    Placement(visible = true, transformation(origin = {-80, 18}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Bassin bassin annotation(
    Placement(visible = true, transformation(origin = {60, 20}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Composants.Command command annotation(
    Placement(visible = true, transformation(origin = {-2, -64}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Composants.Turbine turbine annotation(
    Placement(visible = true, transformation(origin = {-28, 46}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Composants.Vannes vannes annotation(
    Placement(visible = true, transformation(origin = {16, -4}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(mer.S_Mer, command.S_Mer) annotation(
    Line(points = {{-80, 8}, {-80, -64}, {-12, -64}}));
  connect(command.S_Turbines, turbine.S_Turbines) annotation(
    Line(points = {{-6, -54}, {-28, -54}, {-28, 36}}));
  connect(command.S_Vannes, vannes.S_Vannes) annotation(
    Line(points = {{2, -54}, {16, -54}, {16, -14}}));
  connect(command.S_Bassin, bassin.S_Bassin) annotation(
    Line(points = {{8, -64}, {60, -64}, {60, 10}}));
  connect(bassin.vannes, vannes.bassin) annotation(
    Line(points = {{50, 16}, {26, 16}, {26, -4}}));
  connect(vannes.mer, mer.vannes) annotation(
    Line(points = {{6, -4}, {-70, -4}, {-70, 14}}));
  connect(mer.turbines, turbine.mer) annotation(
    Line(points = {{-70, 22}, {-38, 22}, {-38, 46}}));
  connect(turbine.bassin, bassin.turbines) annotation(
    Line(points = {{-18, 46}, {50, 46}, {50, 24}}));
end Usine;
